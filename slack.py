"""
1) Create a team on slack

2) Create two private channels "simple", "advanced"

3) Into "simple" inset The Complete Works of William Shakespeare
from https://raw.githubusercontent.com/bbejeck/hadoop-algorithms/master/src/shakespeare.txt

every line as separate slack message

4) Into "advanced" as "Code or text snippet":
title: "Example title"
type: "Plain Text"
content: "snippet content snippet content"
comment: "firts"

5) Into "advanced" upload some txt file with the content:
---------------------
AAAAAAAAAAAAAAAAAAAAA
BBBBBBBBBBBBBBBBBBBBB
CCCCCCCCCCCCCCCCCCCCC
---------------------

6) create new slack user for your team "second user"

7) add second user to "simple" channel

8) login as second user and check that The Complete Works of William Shakespeare is added
"""

import unittest
import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


USER_EMAIL = None


class SlackTest(unittest.TestCase):
    create_workspace_url = 'https://slack.com/create#email'
    ten_min_mail_url = 'https://10minut.xyz/'

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome('drivers/chromedriver')

    def _get_confirmation_code_from_mailbox(self):
        """
        Check every 5 seconds Confirmation Code on the mailbox
        """
        while True:
            try:
                slack_message = self.driver.find_element_by_partial_link_text('Confirmation Code').text
                code = slack_message.split(':')[1]
                code = code.replace(' ', '').replace('-', '')  # delete all `spaces` and `dashes` from string
                self.assertEqual(len(code), 6)  # code always should have 6 chars
                return code
            except NoSuchElementException:
                time.sleep(5)

    def _insert_to_simple_channel_shakeseare_text(self):
        """
        Insert Shakespeare text to `Simple` channel from text file
        """
        editor = self.driver.find_element_by_xpath("//div[@aria-label='Message #simple']")

        with open('files/shakespeare_short.txt') as f:
            for line in f.readlines():
                editor.send_keys(line)
                editor.submit()

    def test_create_email_for_slack(self):
        global USER_EMAIL
        self.driver.get(self.ten_min_mail_url)
        email_address = self.driver.find_element_by_id('inputEmail3')
        USER_EMAIL = email_address.get_attribute('value')
        self.assertTrue(len(USER_EMAIL) > 10)
        self.assertTrue('@' in USER_EMAIL)

    def test_slack_scenario(self):
        """
        * Open slack page in new window
        * Check that title is correct
        """
        self.driver.execute_script("window.open('{}', 'new_window')".format(self.create_workspace_url))
        self.driver.switch_to.window(self.driver.window_handles[-1])
        self.assertEqual(self.driver.title, 'Create a Workspace | Slack')
        time.sleep(3)

        """
        * Create new slack team/workplace
        """
        email_input = self.driver.find_element_by_id('signup_email')
        email_input.send_keys(USER_EMAIL)
        self.driver.find_element_by_id('submit_btn').click()
        self.driver.switch_to.window(self.driver.window_handles[0])

        """
        * Get confirmation code and paste to slack window
        """
        code = self._get_confirmation_code_from_mailbox()
        self.driver.switch_to.window(self.driver.window_handles[-1])
        first_code_number = self.driver.find_elements_by_class_name('inline_input')[0]
        first_code_number.send_keys(code)
        time.sleep(3)

        """
        * Fill fields `Full name` and `Display name`
        * Submit form
        """
        full_name = self.driver.find_element_by_name('full_name')
        full_name.send_keys('firstuser')
        display_name = self.driver.find_element_by_name('display_name')
        display_name.send_keys('firstuser')
        self.driver.find_element_by_id('submit_btn').click()
        time.sleep(3)

        """
        * Set password for slack account
        * Submit form
        """
        password = self.driver.find_element_by_id('signup_password')
        password.send_keys('firstuser')
        self.driver.find_element_by_id('submit_btn').click()
        time.sleep(5)

        """
        NOTE: must be done manually :(
        -----------
        * Choose correct fields about your company
        * Submit form
        * Set team name and submit
        * Agree slack information
        * Skip not used windows
        """
        self.driver.find_element_by_id('submit_btn').click()
        time.sleep(3)
        company_name = self.driver.find_element_by_name('team_name')
        company_name.send_keys('workspace-exercise-2')
        time.sleep(3)
        self.driver.find_element_by_id('submit_btn').click()
        time.sleep(3)
        self.driver.find_element_by_id('submit_btn').click()
        time.sleep(3)
        self.driver.find_element_by_id('create_tos_i_agree').click()
        time.sleep(3)
        self.driver.find_element_by_id('secondary_btn').click()
        time.sleep(15)

        """
        * Close `welcome` messages
        """
        self.driver.find_element_by_class_name('c-modal__close--onboarding_dialog').click()
        time.sleep(1)
        self.driver.find_element_by_link_text('Skip completely').click()
        time.sleep(3)

        """
        * Create `advanced` chanel
        * Create `simple` chanel
        """
        self.driver.find_element_by_class_name('p-channel_sidebar__section_heading_plus').click()
        time.sleep(3)
        btn = self.driver.find_element_by_id('channel_create_title')
        btn.send_keys('advanced')
        time.sleep(3)
        self.driver.find_element_by_id('save_channel').click()
        time.sleep(3)
        self.driver.find_element_by_class_name('p-channel_sidebar__section_heading_plus').click()
        time.sleep(3)
        btn = self.driver.find_element_by_id('channel_create_title')
        btn.send_keys('simple')
        time.sleep(3)
        self.driver.find_element_by_id('save_channel').click()
        time.sleep(3)

        """
        * Insert text to simple channel 
        """
        try:
            self.driver.find_element_by_partial_link_text('Got it!').click()
        except NoSuchElementException:
            pass
        finally:
            self._insert_to_simple_channel_shakeseare_text()

        """
        * Go to `advanced` channel
        * Upload `snippet` with content
        """
        self.driver.find_element_by_xpath("//a[@aria-label='advanced (channel)']").click()

        try:
            self.driver.find_element_by_partial_link_text('Got it!').click()
        except NoSuchElementException:
            pass

        editor = self.driver.find_element_by_xpath("//div[@aria-label='Message #advanced']")
        editor.send_keys('snippet content snippet content')
        time.sleep(2)
        self.driver.find_element_by_id('primary_file_button').click()
        self.driver.find_element_by_link_text('Code or text snippet').click()
        time.sleep(3)
        title = self.driver.find_element_by_id('client_file_snippet_title_input')
        title.send_keys('Example title')
        textarea = self.driver.find_element_by_xpath("//div[@id='file_comment_textarea']/div/p")
        self.driver.execute_script("arguments[0].innerHTML = 'First';", textarea)
        self.driver.find_element_by_link_text('Create Snippet').click()



